package net.migmac.dinodoomsday;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;

import net.migmac.dinodoomsday.gameobjectspec.GameObjectSpec;

import java.util.Random;

/**
 * This class is used to manipulate the graphics for the falling objects.
 */
class FallingBlockGraphicsComponent implements GraphicsComponent {
    private static final String TAG = "FBGraphics";
    private String[] mBitmapNames;
    private int mColor;
    private String mBitmapKey;
    private GameObjectSpec mObjectSpec;

    @Override
    public void initialize(Context context, GameObjectSpec spec, PointF dpiScaledObjectSize) {

        mBitmapNames = spec.getBitmapNames();
        int bitmapSize = mBitmapNames.length;
        Random random = new Random();
        String bitmapName = mBitmapNames[random.nextInt(bitmapSize - 1)];
        mBitmapKey = bitmapName + (int) spec.getSize().x;
        mColor = spec.getColor();
        mObjectSpec = spec;
        BitmapStore.addBitmap(context, bitmapName, mColor, dpiScaledObjectSize, mBitmapKey);
    }

    @Override
    public void draw(Canvas canvas, Paint paint, Transform transform) {

        Bitmap bitmap = BitmapStore.getBitmap(mBitmapKey);
        Rect screenCoordinates = new Rect((int)transform.getLocation().x, (int)transform.getLocation().y,
                (int)transform.getSize().x, (int)transform.getSize().y);
        canvas.drawBitmap(bitmap, screenCoordinates.left, screenCoordinates.top, paint);
    }
}
