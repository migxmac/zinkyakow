package net.migmac.dinodoomsday;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;

import java.util.HashMap;
import java.util.Map;

/**
 * This class stores the bitmaps used in/for GameObjects.
 */
class BitmapStore {
    private static Map<String, Bitmap> mBitmapsMap;
    private static BitmapStore gameBitmapStore;

    static BitmapStore getInstance(Context context) {
        gameBitmapStore = new BitmapStore(context);
        return gameBitmapStore;
    }

    private BitmapStore(Context context) {
        mBitmapsMap = new HashMap();
        // do we need any default bitmaps?
    }

    static Bitmap getBitmap(String bitmapName) {
        if (mBitmapsMap.containsKey(bitmapName)) {
            return mBitmapsMap.get(bitmapName);
        } else {
            return mBitmapsMap.get("default");
        }
    }

    static void addBitmap(Context context, String bitmapName, int color, PointF dpiScaledObjectSize,
                          String bitmapKey) {
        Bitmap bitmap;
        // if there is no background image, will create one from a color
        if (bitmapName.isEmpty() || bitmapName == null) {
            bitmap = createImage((int) dpiScaledObjectSize.x, (int) dpiScaledObjectSize.y, color);
        } else {
            // make resourceID from file name
            int resID = context.getResources().getIdentifier(bitmapName, "drawable",
                    context.getPackageName());
            // load bitmap using id
            bitmap = BitmapFactory.decodeResource(context.getResources(), resID);
            // resize the bitmap
            bitmap = Bitmap.createScaledBitmap(bitmap, (int) (dpiScaledObjectSize.x),
                    (int) (dpiScaledObjectSize.y), false);
        }

        mBitmapsMap.put(bitmapKey, bitmap);
    }

    static void clearStore() {
        mBitmapsMap.clear();
    }

    /**
     * A one color image.
     * @param width
     * @param height
     * @param color
     * @return A one color image with the given width and height.
     *
     * from gist: https://gist.github.com/catehstn/6fc1a9ab7388a1655175
     */
    public static Bitmap createImage(int width, int height, int color) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setColor(color);
        canvas.drawRect(0F, 0F, (float) width, (float) height, paint);
        return bitmap;
    }

}
