package net.migmac.dinodoomsday;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * This class holds the state of the game, be it paused or started and handles the
 * manipulation of the speed and score.
 */
class GameState {

    private static volatile boolean mThreadRunning = false;
    private static volatile boolean mPaused = true;
    private static volatile boolean mGameOver = true;
    private static volatile boolean mDrawing = false;
    private static volatile float mSpeed;

    private EngineController mEngineController;
    private SharedPreferences.Editor mEditor;
    private int mHighScore;

    private String mCurrentLevel;

    GameState(EngineController ec, Context context) {
        mEngineController = ec;
        SharedPreferences preferences = context.getSharedPreferences("HighScore", Context.MODE_PRIVATE);

        mEditor = preferences.edit();
        mSpeed = preferences.getInt("speed", 50);
    }

    void startNewGame() {
        stopEverything();
        mEngineController.startNewLevel();
        startEverything();
        mEngineController.spawnOne();
    }

    void stopEverything() {
        mPaused = true;
        mGameOver = true;
        mDrawing = false;
    }

    void pauseEverything() {
        mPaused = true;
        mDrawing = false;
        mEditor.apply();
    }

    void unPauseEverything() {
        mPaused = false;
        mDrawing = true;
    }

    private void startEverything() {
        mPaused = false;
        mGameOver = false;
        mDrawing = true;
    }

    void stopThread() {
        mThreadRunning = false;
    }

    boolean getThreadRunning() {
        return mThreadRunning;
    }

    void startThread() {
        mThreadRunning = true;
    }

    boolean getDrawing() {
        return mDrawing;
    }

    boolean getPaused() {
        return mPaused;
    }

    void setPaused() {
        mPaused = true;
    }

    boolean getGameOver() {
        return mGameOver;
    }

    void setCurrentLevel(String level) {
        mCurrentLevel = level;
    }

    String getCurrentLevel() {
        return mCurrentLevel;
    }

    void setSpeed(float animationSpeed) {
        mSpeed = animationSpeed;
        mEditor.putInt("speed", (int) mSpeed);
        mEditor.apply();
    }

    float getSpeed() {
        return mSpeed;
    }

    public void scorePoints(int points) {
        mHighScore += points;
    }

    public int getHighScore() {
        return mHighScore;
    }
}
