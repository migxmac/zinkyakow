package net.migmac.dinodoomsday;

import android.graphics.RectF;
import android.util.Log;

import java.util.ArrayList;

/**
 * This class handles the limited physics of the game, but if there were more
 * interactions between the objects, that would be handled here.
 */
class PhysicsEngine {
    private static final String TAG = "Physics Engine";

    void update(long fps, ArrayList<GameObject> objects, GameState gs) {
        for (GameObject object : objects) {
            if (object.checkActive()) {
                if (object.getTag() != "Background") {
                    float animationSpeed = gs.getSpeed();
                    object.getTransform().setSpeed(animationSpeed);
                }
                object.update(fps);
            }
        }

        detectOffScreen(objects);
    }

    private void detectOffScreen(ArrayList<GameObject> objects) {

        // check if any of the active objects have gone off-screen
        for (GameObject object : objects) {
            if (object.checkActive()) {
                // object active, is its origin still on screen
                if (object.getTransform().getLocation().y > object.getTransform().getScreenHeight()) {

                    object.setInactive();
                }
            }
        }

    }

    public boolean detectCollisionWithObject(ArrayList<GameObject> objects, GameObject target) {

        GameObject go1 = target;
        RectF go1Collider = go1.getTransform().getCollider();

        for (GameObject go2 : objects) {
            if (go2.checkActive() && !go2.equals(go1)) {
                Log.d(TAG, "here");
                if (RectF.intersects(go1Collider, go2.getTransform().getCollider())) {
                    Log.d(TAG, "detectCollisionWithObject()" + go2.getTransform().getCollider().toShortString());
                    Log.d(TAG, "detectCollisionWithObject()" + go1Collider.toShortString());
                    return true;
                }
            }
        }
        return false;
    }
}

