package net.migmac.dinodoomsday;

/**
 * This interface allows components to add themselves as listeners to the GameEngine class.
 */
interface GameEngineBroadcaster {
    void addObserver(InputObserver o);
}
