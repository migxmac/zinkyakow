package net.migmac.dinodoomsday;

import android.content.Context;
import android.graphics.PointF;

import net.migmac.dinodoomsday.gameobjectspec.BackgroundSpec;
import net.migmac.dinodoomsday.gameobjectspec.DinosaurSpec;
import net.migmac.dinodoomsday.levels.Level;
import net.migmac.dinodoomsday.levels.LevelOne;

import java.util.ArrayList;

/**
 * This class handles creation and switching levels.
 * Current game only has one level, but can be expanded.
 */
final class LevelManager {

    private ArrayList<GameObject> objects;
    private Level currentLevel;
    private GameObjectFactory factory;

    LevelManager(Context context, PointF screenSize, GameEngine ge) {
        objects = new ArrayList<>();
        factory = new GameObjectFactory(context, screenSize, ge);
    }

    void setCurrentLevel(String level) {
        switch (level) {
            case "one":
                currentLevel = new LevelOne();
                break;
            default:
                // unexpected, this shouldn't happen
        }
    }

    void buildGameObjects(GameState gs) {
        objects.clear();
        // Starting position is 0,0
        PointF location = new PointF(0, 0);

        // loop through sizes, create objects and add to object array
        // use switch in case want different number of each object size
        // initial level will have 3 of each object size, thus set in default
        int numberOfEachObject = 3;
        ArrayList<String> levelToLoad = currentLevel.getObjectSizes();
        for (int i = 0; i < levelToLoad.size(); i++) {
            switch (levelToLoad.get(i)) {
                case "1":
                    objects.add(factory.create(new BackgroundSpec(), location));
                    break;
                default:
                    int size = Integer.parseInt(levelToLoad.get(i));
                    for (int x = 0; x < numberOfEachObject; x++) {
                        objects.add(factory.create(new DinosaurSpec(new PointF(size, size)), location));
                    }
                    break;
            }
        }
    }

    ArrayList<GameObject> getGameObjects() {
        return objects;
    }

}
