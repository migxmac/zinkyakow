package net.migmac.dinodoomsday;

import android.graphics.PointF;
import android.graphics.RectF;

/**
 * This class is the default transform class.
 * Location and movement is defined here.
 */
class Transform {

    RectF mCollider;
    private PointF mLocation;
    private PointF mScreenSize;
    private float mSpeed;
    private float mObjectHeight;
    private float mObjectWidth;
    private PointF mStartingPosition;
    private boolean mHeadingUp = false;
    private boolean mHeadingDown = false;

    private boolean mFacingRight = true;
    private boolean mHeadingLeft = false;
    private boolean mHeadingRight = false;

    Transform(float speed, float objectWidth, float objectHeight, PointF startingLocation, PointF screenSize) {
        mCollider = new RectF();
        mSpeed = speed;
        mObjectHeight = objectHeight;
        mObjectWidth = objectWidth;
        mLocation = startingLocation;
        mStartingPosition = new PointF(mLocation.x, mLocation.y);
        mScreenSize = screenSize;
    }

    public void updateCollider() {
        mCollider.top = mLocation.y;
        mCollider.left = mLocation.x;
        mCollider.bottom = (mCollider.top + mObjectHeight);
        mCollider.right = (mCollider.left + mObjectWidth);
    }

    public RectF getCollider() {
        return mCollider;
    }

    void headUp() {
        mHeadingUp = true;
        mHeadingDown = false;
    }

    void headDown() {
        mHeadingDown = true;
        mHeadingUp = false;
    }

    boolean headingUp() {
        return mHeadingUp;
    }

    boolean headingDown() {
        return mHeadingDown;
    }

    float getSpeed() {
        return mSpeed;
    }

    void setSpeed(float speed) {
        mSpeed = speed;
    }

    PointF getLocation() {
        return mLocation;
    }

    PointF getSize() {
        return new PointF((int) mObjectWidth, (int)mObjectHeight);
    }

    void setSize(PointF dpiScaledSize) {
        mObjectWidth = dpiScaledSize.x;
        mObjectHeight = dpiScaledSize.y;
    }

    void headRight() {
        mHeadingRight = true;
        mHeadingLeft = false;
        mFacingRight = true;
    }

    void headLeft() {
        mHeadingLeft = true;
        mHeadingRight = false;
        mFacingRight = false;
    }

    boolean headingRight() {
        return mHeadingRight;
    }

    boolean headingLeft() {
        return mHeadingLeft;
    }

    void stopHorizontal() {
        mHeadingLeft = false;
        mHeadingRight = false;
    }

    void stopMovingLeft() {
        mHeadingLeft = false;
    }

    void stopMovingRight() {
        mHeadingRight = false;
    }

    boolean getFacingRight() {
        return mFacingRight;
    }

    PointF getStartingPosition() {
        return mStartingPosition;
    }

    float getScreenHeight() {
        return mScreenSize.y;
    }

    float getScreenWidth() {
        return mScreenSize.x;
    }

    float getObjectHeight() {
        return mObjectHeight;
    }

    float getObjectWidth() {
        return mObjectWidth;
    }

    void setObjectWidth(float width) {
        mObjectWidth = width;
    }

    void setObjectHeight(float height) {
        mObjectHeight = height;
    }

    void setLocation(float horizontal, float vertical) {
        mLocation = new PointF(horizontal, vertical);
        updateCollider();
    }

}
