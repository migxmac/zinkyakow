package net.migmac.dinodoomsday;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * This class is the main activity for the game.
 */
public class GameActivity extends Activity implements View.OnClickListener {

    GameEngine mGameEngine;
    private LinearLayout gameEngineLayout;

    ImageButton actionButton;
    TextView scoreTextView;
    SeekBar speedSeekBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        Display display = getWindowManager().getDefaultDisplay();
        Point screenSize = new Point();
        display.getSize(screenSize);
        gameEngineLayout = findViewById(R.id.mySurface);
        mGameEngine = new GameEngine(this, screenSize);
        gameEngineLayout.addView(mGameEngine);

        actionButton = findViewById(R.id.startButton);
        actionButton.setOnClickListener(this);

        scoreTextView = findViewById(R.id.scoreView);
        mGameEngine.setScoreView(scoreTextView);

        speedSeekBar = findViewById(R.id.speedSeekBar);
        speedSeekBar.setMax(100);
        speedSeekBar.incrementProgressBy(10);
        speedSeekBar.setProgress(50);
        final TextView seekBarValue = findViewById(R.id.seekBarValue);
        speedSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedValue = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progress = progress / 10;
                progress = progress * 10;
                seekBarValue.setText(String.valueOf(progress));
                progressChangedValue = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // change the speed value to progressChangedValue
                mGameEngine.getGameState().setSpeed(progressChangedValue);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGameEngine.startThread();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mGameEngine.stopThread();
    }

    @Override
    public void onClick(View v) {
        mGameEngine.startOrPause();
        if (mGameEngine.getGameState().getPaused()) {
            actionButton.setImageResource(R.drawable.button_start_green);
        } else {
            actionButton.setImageResource(R.drawable.button_pause_green);
        }
    }

}
