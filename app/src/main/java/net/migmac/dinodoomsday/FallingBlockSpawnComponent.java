package net.migmac.dinodoomsday;

import android.util.Log;

import java.util.Random;

/**
 * This class handles how the game object is spawned.  The spawning updates the transform
 * so that the object can be drawn to the canvas on the next update cycle.
 */
class FallingBlockSpawnComponent implements SpawnComponent {
    private static final String TAG = "FBSpawnComponent";

    @Override
    public void spawn(Transform transform, float dpiScaleFactor, int animationSpeed) {
        int spawnHeightMax = (int) transform.getScreenHeight()/5;
        int objectHeight = (int) transform.getObjectHeight();
        int spawnHeightMin = spawnHeightMax - objectHeight;

        int screenWidth = (int) transform.getScreenWidth();
        int objectWidth = (int) transform.getObjectWidth();
        int boundForX = screenWidth - objectWidth;

        // set animationSpeed
        transform.setSpeed(animationSpeed);


        Random random = new Random();
        // spawn just off the screen but within the screen width
        Log.d(TAG, "Object Width: " + objectWidth);

        float xPosition = random.nextInt(boundForX);

        Log.d(TAG, "X Position: " + xPosition);
        // set height to vertically just above the visible game
        float yPosition = random.nextInt(objectHeight) + spawnHeightMin;
        // set the spawn location for the object
        transform.setLocation(xPosition, yPosition);
    }
}
