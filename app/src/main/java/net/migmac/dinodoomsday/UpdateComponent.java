package net.migmac.dinodoomsday;

/**
 * This interface is used to update components.
 */
interface UpdateComponent {
    void update(long fps, Transform t);
}
