package net.migmac.dinodoomsday;

/**
 * This class defines method(s) for handling the spawning of a GameObject.
 */
interface SpawnComponent {

    void spawn(Transform t, float dpiScaleFactor, int animationSpeed);
}
