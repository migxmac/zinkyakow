package net.migmac.dinodoomsday;

import android.graphics.PointF;

/**
 * This class is used to update the game object each frame (cycle).
 */
class FallingBlockUpdateComponent implements UpdateComponent {

    @Override
    public void update(long fps, Transform t) {
        PointF location = t.getLocation();
        if (t.headingDown()) {
            location.y += t.getSpeed() / fps;
        } else {
            // Must be first update of the game
            // so start going down
            t.headDown();
        }

        // Update the colliders with the new position
        t.updateCollider();
    }

}
