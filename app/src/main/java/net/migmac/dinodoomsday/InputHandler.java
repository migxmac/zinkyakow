package net.migmac.dinodoomsday;

import android.graphics.Point;
import android.graphics.RectF;
import android.util.Log;
import android.view.MotionEvent;

import java.util.ArrayList;

/**
 * This class handles the input from the Activity and updates the GameEngine and
 * GameObjects accordingly.
 */
class InputHandler implements InputObserver {
    private static final String TAG = "InputHandler";

    private Point mScreenSize;

    InputHandler(GameEngineBroadcaster broadcaster, Point screenSize) {
        mScreenSize = screenSize;
        // add as an observer
        addObserver(broadcaster);
    }

    void addObserver(GameEngineBroadcaster broadcaster) {
        broadcaster.addObserver(this);
    }

    @Override
    public void handleInput(MotionEvent event, GameState gameState, ArrayList<GameObject> gameObjects) {
        int i = event.getActionIndex();
        float touchX = event.getX(i);
        float touchY = event.getY(i);

        int eventType = event.getAction() & MotionEvent.ACTION_MASK;

        if ((eventType == MotionEvent.ACTION_UP) && !gameState.getPaused()) {
            boolean missed = true;
            for (GameObject object : gameObjects){
                if (object.checkActive()) {
                    RectF objectLocation = object.getTransform().getCollider();
                    if (objectLocation.contains(touchX, touchY)) {
                        // set inactive
                        object.popped();
                        // add points to score;
                        gameState.scorePoints(object.getPointValue());
                        missed = false;
                        break;
                    }
                }
            }
            if (missed && !touchInHud(touchX, touchY)) {
                SoundEngine.playMissDino();
            }
        }
    }
    @Override
    public void startOrPause(GameState gameState) {
        // if game is not paused
        if (!gameState.getPaused()) {
            // pause game
            gameState.pauseEverything();
        } else {
            if (gameState.getGameOver()) {
                // first time starting game, so start the game
                gameState.startNewGame();
            } else {
                // game has been started, but is paused, restart game
                gameState.unPauseEverything();
            }
        }
    }

    private boolean touchInHud(float touchX, float touchY) {
        // User control takes up 1/5 of top of screen
        // only need to modify the y component
        // of screen size
        float hudY = mScreenSize.y / 5;
        RectF hud = new RectF(0,0,mScreenSize.x, hudY);
        return hud.contains(touchX, touchY);
    }
}
