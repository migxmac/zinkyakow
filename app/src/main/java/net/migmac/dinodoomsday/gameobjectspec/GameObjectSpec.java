package net.migmac.dinodoomsday.gameobjectspec;

import android.graphics.PointF;

/**
 * Abstract class for specifying Game Objects
 */
public abstract class GameObjectSpec {
    /**
     * Member variable for the type of object
     */
    private String mTag;
    /**
     * Member variable array to hold bitmap name(s)
     */
    private String[] mBitmapNames;
    /**
     * Member variable used to generate a color bitmap if no
     * bitmap image(s) are provided
     */
    private int mColor;
    /**
     * Member variable indicating speed of game object
     * Might not apply to all objects
     */
    private float mSpeed;
    /**
     * Member variable indicating the size of the object.
     */
    private PointF mSize;
    /**
     * Member variable array holding the components needed to build
     * different game objects
     */
    private String[] mComponents;
    /**
     * Member variable holds the number of frames if the object is animated
     */
    private int mFramesAnimation;

    GameObjectSpec(String tag, String[] bitmapNames,int color, float speed, PointF size, String[] components, int framesAnimation ){

        mTag = tag;
        mBitmapNames = bitmapNames;
        mColor = color;
        mSpeed = speed;
        mSize = size;
        mComponents = components;
        mFramesAnimation = framesAnimation;
    }

    public int getNumFrames(){
        return mFramesAnimation;
    }

    public String getTag(){
        return mTag;
    }

    public String[] getBitmapNames(){
        return mBitmapNames;
    }

    public int getColor() {
        return mColor;
    }

    public float getSpeed() {
        return mSpeed;
    }

    public PointF getSize(){
        return mSize;
    }

    public void setSize(PointF newSize) {
        mSize = newSize;
    }

    public String[] getComponents(){
        return mComponents;
    }
}
