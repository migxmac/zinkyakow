package net.migmac.dinodoomsday.gameobjectspec;

import android.graphics.Color;
import android.graphics.PointF;

/**
 * This class is used to define specifics for the background object
 */
public class BackgroundSpec extends GameObjectSpec {
    // This is all the unique specifications for the background
    private static final String tag = "Background";
    private static final String[] bitmapNames = new String[] {"fire"}; // only one background for now
    private static final int color = Color.argb(255, 0, 102, 51);
    private static final int framesOfAnimation = 1;
    private static final float speed = 0f;
    private static final PointF size = new PointF(1920, 2400);
    private static final String[] components = new String [] {
            "BackgroundGraphicsComponent","BackgroundUpdateComponent"};

    public BackgroundSpec() {
        super(tag, bitmapNames, color, speed, size, components, framesOfAnimation);
    }
}