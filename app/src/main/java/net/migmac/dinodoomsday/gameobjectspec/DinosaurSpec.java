package net.migmac.dinodoomsday.gameobjectspec;

import android.graphics.Color;
import android.graphics.PointF;

/**
 * This class is used to define the specifications for the dinosaur game object
 */
public class DinosaurSpec extends GameObjectSpec {
    private static final String tag = "Dinosaur";
    private static final String[] bitmapName = new String [] {"tricer", "trex", "stego", "spina"};
    private static final int color = Color.argb(255, 255, 0, 0);
    private static final int framesOfAnimation = 1;
    private static final float speed = 50f;

    private static final String[] components = new String [] {
            "FallingBlockUpdateComponent", "FallingBlockGraphicsComponent", "FallingBlockSpawnComponent"};

    // specify size when creating object
    public DinosaurSpec(PointF size) {
        super(tag, bitmapName, color, speed, size, components, framesOfAnimation);
    }
}
