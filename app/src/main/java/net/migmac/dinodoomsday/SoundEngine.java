package net.migmac.dinodoomsday;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * This class hold and plays all the sounds in the game.
 */
class SoundEngine {
    private static final String TAG = "SoundEngine";
    // for playing sound effects
    private static SoundPool mSoundPool;
    private static int squeek1_ID = -1;
    private static int squeek2_ID = -1;
    private static int squeek3_ID = -1;
    private static int squeek4_ID = -1;
    private static int squeek5_ID = -1;
    private static int squeek6_ID = -1;

    private static int ouch1_ID = -1;
    private static int ouch2_ID = -1;
    private static int ouch3_ID = -1;
    private static int ouch4_ID = -1;
    private static int ouch5_ID = -1;
    private static int ouch6_ID = -1;

    private static int roar1_ID = -1;
    private static int roar2_ID = -1;
    private static int roar3_ID = -1;
    private static int roar4_ID = -1;
    private static int roar5_ID = -1;
    private static int roar6_ID = -1;

    private static SoundEngine gameSoundEngine;

    private static ArrayList<Integer> mTouchSoundIds = new ArrayList<>();
    private static ArrayList<Integer> mMissSoundIds = new ArrayList<>();

    private static Random random = new Random();

    public static SoundEngine getInstance(Context context) {
        gameSoundEngine = new SoundEngine(context);
        return gameSoundEngine;
    }

    private SoundEngine(Context context) {
        // initialize SoundPool
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();

            mSoundPool = new SoundPool.Builder()
                    .setMaxStreams(5)
                    .setAudioAttributes(audioAttributes)
                    .build();
        } else {
            mSoundPool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        }

        try {
            AssetManager assetManager = context.getAssets();
            AssetFileDescriptor descriptor;

            // prepare the sounds in memory
            descriptor = assetManager.openFd("squeekBoy1.ogg");
            squeek1_ID = mSoundPool.load(descriptor, 0);
            mTouchSoundIds.add(squeek1_ID);

            descriptor = assetManager.openFd("squeekBoy2.ogg");
            squeek2_ID = mSoundPool.load(descriptor, 0);
            mTouchSoundIds.add(squeek2_ID);

            descriptor = assetManager.openFd("squeekGirl1.ogg");
            squeek3_ID = mSoundPool.load(descriptor, 0);
            mTouchSoundIds.add(squeek3_ID);

            descriptor = assetManager.openFd("squeekGirl2.ogg");
            squeek4_ID = mSoundPool.load(descriptor, 0);
            mTouchSoundIds.add(squeek4_ID);

            descriptor = assetManager.openFd("squeekDad1.ogg");
            squeek5_ID = mSoundPool.load(descriptor, 0);
            mTouchSoundIds.add(squeek5_ID);

            descriptor = assetManager.openFd("squeekDad2.ogg");
            squeek6_ID = mSoundPool.load(descriptor, 0);
            mTouchSoundIds.add(squeek6_ID);

            descriptor = assetManager.openFd("ouchGirl1.ogg");
            ouch1_ID = mSoundPool.load(descriptor, 0);
            mTouchSoundIds.add(ouch1_ID);

            descriptor = assetManager.openFd("ouchGirl2.ogg");
            ouch2_ID = mSoundPool.load(descriptor, 0);
            mTouchSoundIds.add(ouch2_ID);

            descriptor = assetManager.openFd("ouchBoy1.ogg");
            ouch3_ID = mSoundPool.load(descriptor, 0);
            mTouchSoundIds.add(ouch3_ID);

            descriptor = assetManager.openFd("ouchBoy2.ogg");
            ouch4_ID = mSoundPool.load(descriptor, 0);
            mTouchSoundIds.add(ouch4_ID);

            descriptor = assetManager.openFd("ouchDad1.ogg");
            ouch5_ID = mSoundPool.load(descriptor, 0);
            mTouchSoundIds.add(ouch5_ID);

            descriptor = assetManager.openFd("ouchDad2.ogg");
            ouch6_ID = mSoundPool.load(descriptor, 0);
            mTouchSoundIds.add(ouch6_ID);

            descriptor = assetManager.openFd("grrrGirl1.ogg");
            roar1_ID = mSoundPool.load(descriptor, 0);
            mMissSoundIds.add(roar1_ID);

            descriptor = assetManager.openFd("grrrGirl2.ogg");
            roar2_ID = mSoundPool.load(descriptor, 0);
            mMissSoundIds.add(roar2_ID);

            descriptor = assetManager.openFd("grrrBoy1.ogg");
            roar3_ID = mSoundPool.load(descriptor, 0);
            mMissSoundIds.add(roar3_ID);

            descriptor = assetManager.openFd("grrrBoy2.ogg");
            roar4_ID = mSoundPool.load(descriptor, 0);
            mMissSoundIds.add(roar4_ID);

            descriptor = assetManager.openFd("grrrDad1.ogg");
            roar5_ID = mSoundPool.load(descriptor, 0);
            mMissSoundIds.add(roar5_ID);

            descriptor = assetManager.openFd("grrrDad2.ogg");
            roar6_ID = mSoundPool.load(descriptor, 0);
            mMissSoundIds.add(roar6_ID);

        } catch (IOException e) {
            // error
        }
    }

    public static void playMissDino() {
        Log.d(TAG, "Miss");
        mSoundPool.play(randomMissSoundId(), 1, 1, 0, 0, 1);
    }

    public static void playTouchDino() {
        Log.d(TAG, "Touch");
        mSoundPool.play(randomTouchSoundId(), 1, 1, 0, 0, 1);
    }

    private static int randomTouchSoundId() {
        int sizeTouchIdArray = mTouchSoundIds.size();
        int randomInt = random.nextInt(sizeTouchIdArray - 1);
        return mTouchSoundIds.get(randomInt);
    }

    private static int randomMissSoundId() {
        int sizeMissIdArray = mMissSoundIds.size();
        int randomInt = random.nextInt(sizeMissIdArray - 1);
        return mMissSoundIds.get(randomInt);
    }

}
