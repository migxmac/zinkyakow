package net.migmac.dinodoomsday.levels;

import java.util.ArrayList;

public abstract class Level {
    // extend this class to build new levels
    ArrayList<String> objectSizes;

    public ArrayList<String> getObjectSizes() { return objectSizes; }
}
