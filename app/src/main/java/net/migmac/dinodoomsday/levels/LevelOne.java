package net.migmac.dinodoomsday.levels;

import java.util.ArrayList;

public final class LevelOne extends Level {
    public LevelOne() {
        objectSizes = new ArrayList<>();
        // add 1 which will initiate the background
        objectSizes.add("1");
        // need to add object sizes from 10 to 100px in diameter
        for (int i = 10; i <= 100; i += 10) {
            objectSizes.add(Integer.toString(i));
        }
    }
}
