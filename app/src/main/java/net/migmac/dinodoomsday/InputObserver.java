package net.migmac.dinodoomsday;

import android.view.MotionEvent;

import java.util.ArrayList;

/**
 * This interface allows InputObservers to be called by GameEngine to handle input.
 */
interface InputObserver {
    void handleInput(MotionEvent event, GameState gameState, ArrayList<GameObject> gameObjects);
    void startOrPause(GameState gameState);
}
