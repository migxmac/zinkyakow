package net.migmac.dinodoomsday;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;

import net.migmac.dinodoomsday.gameobjectspec.GameObjectSpec;

/**
 * This class implements methods needed to modify the background.
 */
class BackgroundGraphicsComponent implements GraphicsComponent {

    private String mBitmapName;
    private String mBitmapKey;

    @Override
    public void initialize(Context context, GameObjectSpec spec, PointF objectSize) {

        mBitmapName = spec.getBitmapNames()[0];
        mBitmapKey = mBitmapName + (int) objectSize.x;

        BitmapStore.addBitmap(context, mBitmapName, spec.getColor(), objectSize, mBitmapKey);
    }

    @Override
    public void draw(Canvas canvas, Paint paint, Transform t) {

        Bitmap bitmap = BitmapStore.getBitmap(mBitmapKey);
        canvas.drawBitmap(bitmap, 0, 0, paint);

    }
}