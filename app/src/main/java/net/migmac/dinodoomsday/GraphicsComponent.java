package net.migmac.dinodoomsday;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;

import net.migmac.dinodoomsday.gameobjectspec.GameObjectSpec;

/**
 * This class defines methods for manipulating the graphics for a GameObject.
 */
interface GraphicsComponent {
    /**
     * Initializes the graphics component.
     * @param context
     * @param spec
     * @param objectSize
     */
    void initialize(Context context, GameObjectSpec spec, PointF objectSize);

    /**
     * Draws the component to the canvas.
     * @param canvas
     * @param paint
     * @param transform
     */
    void draw(Canvas canvas, Paint paint, Transform transform);

}
