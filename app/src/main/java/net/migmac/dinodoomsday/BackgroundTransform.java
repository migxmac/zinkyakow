package net.migmac.dinodoomsday;

import android.graphics.PointF;

/**
 * This class handles any background transformations (movement).
 * This could be used to implement a scrolling/moving background.
 */
public class BackgroundTransform extends Transform {

    public BackgroundTransform(float speed, float objectWidth, float objectHeight, PointF startingLocation, PointF screenSize) {
        super(speed, objectWidth, objectHeight, startingLocation, screenSize);

    }
}
