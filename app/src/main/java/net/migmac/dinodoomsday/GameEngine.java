package net.migmac.dinodoomsday;

import android.content.Context;
import android.graphics.Point;
import android.graphics.PointF;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * This class is the game controller.
 */
class GameEngine extends SurfaceView implements Runnable, EngineController, GameEngineBroadcaster {

    private Thread mThread = null;
    private long mFPS;
    private TextView mScoreDisplayTextView;
    private GameState mGameState;
    InputHandler mInputHandler;

    // this ArrayList can be accessed from either thread
    private CopyOnWriteArrayList<InputObserver> inputObservers = new CopyOnWriteArrayList<>();
    LevelManager mLevelManager;
    PhysicsEngine mPhysicsEngine;
    Renderer mRenderer;

    public GameEngine(Context context, Point screenSize) {
        super(context);
        //instantiating singletons
        BitmapStore bitmapStore = BitmapStore.getInstance(context);
        SoundEngine soundEngine = SoundEngine.getInstance(context);

        mGameState = new GameState(this, context);
        mInputHandler = new InputHandler(this, screenSize);
        mPhysicsEngine = new PhysicsEngine();
        mRenderer = new Renderer(this, screenSize);
        mLevelManager = new LevelManager(context, new PointF(screenSize.x, screenSize.y), this);
    }

    @Override
    public void run() {
        long spawnTimer = System.currentTimeMillis();
        long spawnDelta;

        while (mGameState.getThreadRunning()) {
            // used to calculate FPS
            long frameStartTime = System.currentTimeMillis();

            ArrayList<GameObject> objects = mLevelManager.getGameObjects();


            if (!mGameState.getPaused()) {
                mPhysicsEngine.update(mFPS, mLevelManager.getGameObjects(), mGameState);
            }

            spawnDelta = System.currentTimeMillis() - spawnTimer;
            if (spawnDelta >= 1000) {
                spawnOne();
                spawnTimer = System.currentTimeMillis();
            }

            mRenderer.draw(objects, mGameState);

            long timeThisFrame = System.currentTimeMillis() - frameStartTime;

            if (timeThisFrame >= 1) {
                final int MILLIS_IN_SECOND = 1000;
                mFPS = MILLIS_IN_SECOND / timeThisFrame;
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {
        ArrayList<GameObject> objects = mLevelManager.getGameObjects();
        for (InputObserver observer : inputObservers) {
            observer.handleInput(motionEvent, mGameState, objects);
            updateScore();
        }
        return true;
    }

    public void startOrPause() {
        for (InputObserver observer : inputObservers) {
            observer.startOrPause(mGameState);
        }
    }

    public void stopThread() {
        mGameState.stopEverything();
        mGameState.stopThread();
        try {
            mThread.join();
        } catch (InterruptedException e) {
            Log.e("Exception", "stopThread()" + e.getMessage());
        }
    }

    public void startThread() {
        mGameState.startThread();
        mThread = new Thread(this);
        mThread.start();
    }

    public void addObserver(InputObserver observer) {
        inputObservers.add(observer);
    }


    @Override
    public void startNewLevel() {
        // clear the bitmap store
        BitmapStore.clearStore();
        // clear all observers and add the UI observer back
        // when we call buildGameObjects the player's observer will be added too
        inputObservers.clear();
        mGameState.setCurrentLevel("one");
        mInputHandler.addObserver(this);
        mLevelManager.setCurrentLevel(mGameState.getCurrentLevel());
        mLevelManager.buildGameObjects(mGameState);
    }

    public void spawnOne() {
        int animationSpeed;
        Random random = new Random();
        ArrayList<GameObject> objects = mLevelManager.getGameObjects();

        int tries = 0;
        while (tries < objects.size()) {
            int objectIndexToSpawn = random.nextInt(objects.size() - 1);
            GameObject object = objects.get(objectIndexToSpawn);
            if (object.getTag() != "Background" && !object.checkActive()) {
                animationSpeed = (int) object.getTransform().getSpeed();
                object.spawn(animationSpeed);
                if (mPhysicsEngine.detectCollisionWithObject(objects, object)) {
                    object.setInactive();
                } else {
                    break;
                }
            }
            tries++;
        }
    }

    public GameState getGameState() {
        return mGameState;
    }

    public void updateScore() {
        mScoreDisplayTextView.setText(String.valueOf(mGameState.getHighScore()));
    }

    public void setScoreView(TextView scoreTextView) {
        mScoreDisplayTextView = scoreTextView;
    }
}
