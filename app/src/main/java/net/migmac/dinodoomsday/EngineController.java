package net.migmac.dinodoomsday;

/**
 * Interface that allows communication between the GameState and the GameEngine.
 * This will allow the GameState to trigger a new level using the GameEngine class.
 */
interface EngineController {
    void startNewLevel();
    void spawnOne();
}
