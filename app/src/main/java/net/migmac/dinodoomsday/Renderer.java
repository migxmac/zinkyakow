package net.migmac.dinodoomsday;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;

/**
 * This class renders the canvas to the display each frame.
 */
class Renderer {

    private Canvas mCanvas;
    private SurfaceHolder mSurfaceHolder;
    private Paint mPaint;

    Renderer(SurfaceView surfaceView, Point screenSize) {
        mSurfaceHolder = surfaceView.getHolder();
        mPaint = new Paint();
    }

    void draw(ArrayList<GameObject> objects, GameState gameState) {

        if (mSurfaceHolder.getSurface().isValid()) {
            mCanvas = mSurfaceHolder.lockCanvas();
            mCanvas.save();

            if (!gameState.getPaused() && gameState.getDrawing()) {
                for (GameObject object : objects) {
                    if (object.getTag() == "Background"){
                        object.draw(mCanvas, mPaint);
                    }
                    if (object.checkActive()) {
                        float speed = object.getTransform().getSpeed();
                        object.getTransform().setSpeed(speed);
                        object.draw(mCanvas, mPaint);
                    }
                }
            }

            mCanvas.restore();
            mSurfaceHolder.unlockCanvasAndPost(mCanvas);
        }
    }
}
