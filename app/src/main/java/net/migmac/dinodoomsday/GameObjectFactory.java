package net.migmac.dinodoomsday;

import android.content.Context;
import android.graphics.PointF;
import android.util.Log;

import net.migmac.dinodoomsday.gameobjectspec.GameObjectSpec;

/**
 * This class is a factory for generating all game objects.
 */
class GameObjectFactory {

    private static final String TAG = "GameObjectFactory";
    private Context mContext;
    private PointF mScreenSize;
    private GameEngine mGameEngineReference;
    private float mDpiScaleFactor;

    GameObjectFactory(Context context, PointF screenSize, GameEngine gameEngine) {
        mContext = context;
        mScreenSize = screenSize;
        mGameEngineReference = gameEngine;
        // Get the screen's density scale
        mDpiScaleFactor = context.getResources().getDisplayMetrics().density;
    }

    GameObject create(GameObjectSpec spec, PointF location) {
        GameObject object = new GameObject();

        int mNumComponents = spec.getComponents().length;
        object.setTag(spec.getTag());

        // first give the game object the right kind of transform

        switch (object.getTag()) {
            case "Background":
                object.setTransform(new BackgroundTransform(spec.getSpeed(), spec.getSize().x,
                        spec.getSize().y, location, mScreenSize));
                break;

            default: // normal transform
                float speed = mGameEngineReference.getGameState().getSpeed();
                object.setTransform(new Transform(speed, spec.getSize().x, spec.getSize().y, location, mScreenSize));
                object.setPointValue(10 - (int) spec.getSize().x / 10 + 1);
                Log.d(TAG, "Object Point Value: " + object.getPointValue());
                break;
        }

        // loop through and add/initialize all the components
        for (int i = 0; i < mNumComponents; i++) {
            switch (spec.getComponents()[i]) {

                case "FallingBlockUpdateComponent":
                    object.setMovement(new FallingBlockUpdateComponent());
                    break;
                case "FallingBlockGraphicsComponent":
                    setDpiScaleObjectSize(object, spec);
                    object.setGraphics(new FallingBlockGraphicsComponent(),
                            mContext, spec, object.getTransform().getSize(), mDpiScaleFactor);
                    break;
                case "FallingBlockSpawnComponent":
                    object.setSpawner(new FallingBlockSpawnComponent());
                    break;
                case "BackgroundGraphicsComponent":
                    object.setGraphics(new BackgroundGraphicsComponent(),
                            mContext, spec, mScreenSize, mDpiScaleFactor);
                    break;

                default:
                    // error unidentified component
                    break;
            }
        }

        // return the completed GameObject to the LevelManager class
        return object;
    }

    void setDpiScaleObjectSize(GameObject object, GameObjectSpec spec) {
        object.getTransform().setObjectWidth(spec.getSize().x * mDpiScaleFactor);
        object.getTransform().setObjectHeight(spec.getSize().y * mDpiScaleFactor);
    }
}
