package net.migmac.dinodoomsday;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;

import net.migmac.dinodoomsday.gameobjectspec.GameObjectSpec;

/**
 * This class is the base class for each object in the game.
 */
class GameObject {

    private Transform mTransform;
    private boolean mActive = false;
    private String mTag;
    float mDpiScaleFactor;
    private int mPointValue;

    private GraphicsComponent mGraphicsComponent;
    private UpdateComponent mUpdateComponent;
    private SpawnComponent mSpawnComponent;

    void setSpawner(SpawnComponent s) {
        mSpawnComponent = s;
    }

    void setGraphics(GraphicsComponent graphicsComponent, Context context,
                     GameObjectSpec gameObjectSpec, PointF objectSize, float dpiScaleFactor) {

        mDpiScaleFactor = dpiScaleFactor;
        mGraphicsComponent = graphicsComponent;
        graphicsComponent.initialize(context, gameObjectSpec, objectSize);
    }

    void setMovement(UpdateComponent m) {
        mUpdateComponent = m;
    }

    void setTransform(Transform t) {
        mTransform = t;
    }

    void setPointValue(int pointValue) {
        mPointValue = pointValue;
    }

    void draw(Canvas canvas, Paint paint) {
        mGraphicsComponent.draw(canvas, paint, mTransform);
    }

    void update(long fps) {
        mUpdateComponent.update(fps, mTransform);
    }

    boolean spawn(int animationSpeed) {
        if (!mActive) {
            mSpawnComponent.spawn(mTransform, mDpiScaleFactor, animationSpeed);
            mActive = true;
            return true;
        }
        return false;
    }

    boolean checkActive() {
        return mActive;
    }

    String getTag() {
        return mTag;
    }

    void setInactive() {
        mActive = false;
    }

    void popped () {
        setInactive();
        SoundEngine.playTouchDino();
    }

    Transform getTransform() {
        return mTransform;
    }

    void setTag(String tag) {
        mTag = tag;
    }

    public int getPointValue() {
        return mPointValue;
    }
}
